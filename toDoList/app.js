//jshint esversion:6

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
let taskList = ["Aws Developer", "FullStack", "CKA"];

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static("public"));

app.set("view engine", "ejs");

app.get('/', function(req, res){
    let today = new Date();

    let options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
    };

    let day = today.toLocaleDateString("fr-FR", options);

    res.render("list", {
        listTitle: day,
        newListTask: taskList 
    });

});

app.post("/", function(req,res){

    let task = req.body.newTask;
    taskList.push(task);
    res.redirect("/");

});

app.listen(3000, function(){
    console.log("Server starting on port 3000");
});
